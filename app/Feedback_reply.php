<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Feedback_reply extends Model
{
    protected $fillable = [
    	'feedback_id',
    	'reply'
    ];
}
