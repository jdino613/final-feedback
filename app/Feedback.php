<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = array(
        'name',
        'order_id',
        'feedback',
        'user_id'
    );


    public function feedback(){
    	return $this->hasMany('App\Feedback_reply');
    }
}
