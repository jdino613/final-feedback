<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Product;
use App\Order;
use App\User;

class OrderController extends Controller
{

    public function checkout(){
        // Save the transaction in the order table
        // Make sure that there is a user logged in
        // Populate our item_order;

        if(Auth::user()){
            $order = new Order;
            $order->user_id = Auth::user()->id;
            $order->status_id = 1; //pending
            $order->payment_id = 1; //COD
            $order->total = 0;
            $order->save();

            $cart = Session::get('cart');

            $total = 0;
            foreach($cart as $itemId => $quantity){
                // syntax for adding entries to our many to many table
                $order->items()->attach($itemId, ["quantity"=>$quantity]);

                $item = Item::find($itemId);
                $total += $item->price * $quantity;
            }

            $order->total = $total;
            $order->save();

            Session::forget('cart');
            Session::flash('message', "Order successfully placed");
            return redirect('/catalog');
        }else{
            return redirect('/login');
        }
    }


    public function showHistory(){
            $orders = Order::all();
            $products = Product::all();

            return view('userviews.history', compact('orders', 'products'));
        }

    public function cancelOrderByAdmin($id){
            $order = Order::find($id);

            if(Auth::user()->role_id ==2){
                $order->status_id = 3;
                $order->save();

                return redirect('/showorders');
            }else{
                $order->status_id = 4;
                $order->save();

                return redirect("/allorders");
            }

            $order->status_id = 4;
            $order->save();

            return redirect()->back();
        }
    // Index Function
    public function index($id) {
        // $restaurant = User::find($id);
        $consumer = User::find($id);
        // $orders = $restaurant->orders;
        $orders=Order::all();
        // $consumer=User::all();
        return view('orders.index', compact('consumer', 'orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // Store function -> Storing all the information of the Order into the DB
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'product_id' => 'required',
        ]);

        $order = new Order();
        $order->user_id = $request->user_id;
        $order->product_id = $request->product_id;
        $order->save();

        return redirect("orders/".$order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

   
    public function show($id) {
        $order = DB::table('products')
        ->join('orders', 'orders.product_id', '=', 'products.id')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->whereRaw('orders.id = ?', array($id))
        ->select('products.name as product', 'users.name as user', 'users.address as address', 'orders.created_at as time', 'users.id as id')->first();

        // $user = DB::table('products')
        // ->join('users', 'users.id', '=', 'products.user_id')
        // ->select('users.name as user', 'users.address as address', 'users.id as id');
        return view('orders.show')->with('order', $order);
    }


}
