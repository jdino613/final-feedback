<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // Returning all the restaurants created with the role 'restaurant'.
    public function index()
    {
        $restaurants = User::where('role', 'restaurant')->get();
        return view('home')->with('restaurants',$restaurants);
    }
}
