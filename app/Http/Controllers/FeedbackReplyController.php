<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Feedback_reply;
use Illuminate\Support\Facades\Auth;

class FeedbackReplyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            Reply::create([
                'feedback_id' => $request->input('feedback_id'),
                'feedback_reply' => $request->input('feedback_reply'),
                // 'user_id' => Auth::user()->id
            ]);

            return redirect()->route('home')->with('success','Reply added');
        }

        return back()->withInput()->with('error','Something wrong');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback_reply $feedback_reply)
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback_reply $feedback_reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback_reply $feedback_reply)
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback_reply $feedback_reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback_reply $feedback_reply)
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback_reply $feedback_reply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback_reply $feedback_reply)
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback_reply $feedback_reply)
    {
        if (Auth::check()) {
            $reply = Reply::where(['id'=>$feedback_reply->id,'user_id'=>Auth::user()->id]);
            if ($feedback_reply->delete()) {
                return 1;
            }else{
                return 2;
            }
        }else{

        }
        return 3;
    }



}
