<?php

namespace App\Http\Controllers;

use App\User;
use App\Feedback;
use App\Product;
use App\Order;
use App\Profile;
use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Image;
use Auth;
use Session;
// use Illuminate\Foundation\Auth\RegistersUsers;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function edit(User $user)
    {   
        $user = Auth::user();
        return view('edit', compact('user', $user));
    }


    public function update_image(Request $request){

        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $imageName = $user->id.'_image'.time().'.'.request()->image->getClientOriginalExtension();

        $request->image->storeAs('images',$imageName);

        $user->image = $imageName;
        $user->save();

        return back()
            ->with('success','You have successfully upload image.');

    }



    public function update(User $user, Request $request)
    { 
        if(Auth::user()->email == request('email')) {
        
        $this->validate(request(), [
                'name' => 'required',
              //  'email' => 'required|email|unique:users',

                'password' => 'required|min:4|confirmed'
            ]);
        
            $user->name = request('name');
           // $user->email = request('email');

            $user->password = bcrypt(request('password'));

            $image = $request->file('image');
            $imageName = time().".".$image->getClientOriginalExtension();
            $destination="images/";
            $image->move($destination, $imageName);
            $user->image=$destination.$imageName;


            $user->save();
            return back();

            Session::flash("message", "You have updated your settings!");
            return redirect('profile/Auth($id)');

            
        }
        else{
            
        $this->validate(request(), [
                'name' => 'required',
                //'email' => 'required|email|unique:users',
                'email' => 'email|required|unique:users,email,'.$this->segment(2),
                'password' => 'required|min:4|confirmed'
            ]);
            $user->image = request('image');
            $user->name = request('name');
            $user->email = request('email');
            $user->password = bcrypt(request('password'));
            $user->save();
            return back();  

        //     Session::flash("message", "Saved Changes!");
        // return redirect('edit/{user}');
            
        }
    }

    public function profile($id){
        $user=User::find($id);
        $profiles=Profile::where('user_id', Auth::user()->id)->first();

        return view('profile', compact('profiles', 'user'));
    }

    public function addprofile(){
        $user=User::all();

        return view('addprofile', compact('user'));
    }

    public function add_profile(Request $reqst){
        $rules=array(

            // "categories" => "required",
            "phone" => "required",
            "hours" => "required",
            "description" => "required"

        );

        $this->validate($reqst, $rules);

        $new_profile=new Profile;
        $new_profile->phone=$reqst->phone;
        $new_profile->hours=$reqst->hours;
        $new_profile->description=$reqst->description;
        $new_profile->user_id=Auth::user()->id;
        $new_profile->save();

        Session::flash("message", "Your profile is complete!");
        return redirect('profile/Auth($id)');
    }

    public function updateprofile($id){
        $profile=Profile::find($id);
        
        return view('updateprofile', compact('profile'));
    }

    public function update_profile($id, Request $reqst){
        $profile=Profile::find($id);

        $rules = array(

            // "categories" => "required",
            "phone"=>"required",
            "hours"=>"required",
            "description"=>"required"

        );

        $this->validate($reqst, $rules);

        $profile->phone=$reqst->phone;
        $profile->hours=$reqst->hours;
        $profile->description=$reqst->description;

        $profile->save();

        Session::flash("message", "You have updated your profile!");
        return redirect('profile/Auth($id)');
    }


   
}
