<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Product Class
class Product extends Model
{
     public function orders(){
    	return $this->belongsToMany("\App\Order");
    }

    public function products(){
    	return $this->belongsTo("\App\Order")->withPivot("quantity")->withTimeStamps();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
