@extends('layouts.app')
@section('content')

<h1 class="text-center">Profile</h1>

	<div class="col-lg-8 offset-lg-2">
		<form action="/update_profile/{{$profile->id}}" method="POST" class="form-group">
			@csrf
			@method('PATCH')
			<div class="my-1">
				<label for="phone">Phone number</label>
				<input type="number" name="phone" class="form-control" value="{{$profile->phone}}">
			</div>
			<div class="my-1">
				<label for="hours">Hours</label>
				<input type="hours" name="hours" class="form-control" value="{{$profile->hours}}">
			</div>
			<div class="my-1">
				<label for="description">Description</label>
				<textarea name="description" class="form-control" rows="5">{{$profile->description}}</textarea>
			</div>
			<div class="text-center">
				<button class="btn btn-info my-3">Update Profile</button>
			</div>
		</form>
	</div>

@endsection