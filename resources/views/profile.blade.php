@extends('layouts.app')

@section('content')


@if(Session::has("message"))
    <h4>{{ Session::get('message') }}</h4>
@endif

@if(isset($profiles))
<h1 class="text-center">Profile</h1>
    <div class="container d-flex justify-content-center align-items-center flex-column">
        
            
            {{-- @foreach ($restaurants ?? '' as $restaurant)
            <div class="row justify-content-center">
                <img class="card-img-top" src="{{asset($restaurant->image)}}" alt="Card image cap">
            </div>
            @endforeach --}}
            
            <div class="card-body text-center">
                <p>Phone Number: {{$profiles->phone}}</p>
            </div>
            <div class="card-body text-center">
                <p>Hours: {{$profiles->hours}}</p>
            </div>
            <div class="card-body text-center">
                <p>Description: {{$profiles->description}}</p>
            </div>
            <div class="text-center">
                <a href="/update_profile/{{$profiles->id}}" class="btn btn-info">Update Profile</a>
            </div>
        
    </div>
@else
        <div class="text-center">
                <a href="/add_profile" class="btn btn-info my-1">Add A Profile</a>
        </div>
@endif



{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="padding-top:20px;">
            <div class="card">
                <div class="card-header"  style="text-align: center;">{{ __('Profile') }}</div>

                <div class="card-body">
                    
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="name" value="{{ $user->name }}" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <img class="card-img-top" src="{{asset($restaurant->image)}}" alt="Card image cap">
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="email">Email address:</label>
                            <div class="col-md-6">                           
                              <input type="email" name="email" class="form-control" value="{{ $user->email }}" id="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" placeholder="Enter password" name="password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="password">Confirm Password:</label>
                            <div class="col-md-6"> 
                              <input type="password" placeholder="Confirm password" class="form-control" type="password" name="password_confirmation" id="password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save Changes') }}
                                </button>

                            </div>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection