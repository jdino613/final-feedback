<!DOCTYPE html>
<html>
    <head>
        <title> @yield('title')</title>
        <meta charset="utf-8">
        <!-- Boostrap -->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <link rel="stylesheet" type="text/css" href="{{secure_asset('css/styles.css')}}">
    </head>

    
    <body>


        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="{{url('/welcome')}}">FeedBack</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav mr-auto">
              {{-- <li class="nav-item active">
                <a class="nav-link mr-4" href="{{url("/")}}">HOME <span class="sr-only">(current)</span></a>
              </li> --}}
              <li class="nav-item">
                <a class="nav-link mr-4" href="{{url("about")}}">ABOUT</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">{{ __('RESTAURANTS') }}</a>
              </li>
            </ul>
          </div>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
              @if(auth()->check() && auth()->user()->role == "consumer")
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/history') }}">{{ __('VIEW ORDERS') }}</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/showcart') }}">{{ __('CART') }}</a>
              </li>

              @endif
                            <!-- Authentication Links -->
                @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('REGISTER') }}</a>
                            </li>
                        @endif
                    @else
                        @if(Auth::user()->role == "restaurant")  
                            <li>
                                <a class="nav-link" href="{{ url('/restaurant/'.Auth::user()->id) }}">{{ __('Available Menu') }}</a>
                            </li> 
                            <li>
                            <a class="nav-link" href="{{ url('/newproduct') }}">{{ __('Add Food') }}</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ url('/restaurant/'.Auth::user()->id.'/orders') }}">{{ __('View Orders') }}</a>
                            </li>
                        @endif
                        
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}<span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/edit/'.Auth::user()->id) }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('edit-form').submit();">
                                    {{ __('Account Settings') }}
                                </a>
                                <form id="edit-form" action="{{ url('/edit/'.Auth::user()->id) }}" style="display: none;">
                                    @csrf
                                </form>


                                @if((auth()->check() && auth()->user()->role == "restaurant") || (auth()->check() && auth()->user()->role == "consumer"))
                                <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->id) }}"
                                    >
                                    {{ __('Profile') }}
                                </a>
                                <form id="edit-form" action="{{ url('profile/'.Auth::user()->id) }}" style="display: none;">
                                    @csrf
                                </form>      
                                @endif

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
          </nav>
          
          <div class="contentContainer">
                @yield('content')
          </div>
    </body>
</html>

