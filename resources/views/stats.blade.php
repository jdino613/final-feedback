@extends('layouts.app')

@section('title')
    Statistic
@endsection('title')

@section('content')
    <div class="documentationTitle">
        <h3>Donations from {{$restaurant->getAttribute('name')}}</h3>
    </div>
    <div class="container" style="padding-top: 20px; margin-top: 20px; text-align: center;">
        <div class="col-xl" style="padding-top: 20px; padding-bottom: 20px;">
            <p>Total amount of food donations: {{-- {{ $order ?? ''->quantity}} --}}</p>
        </div>
    </div>
@endsection('content')