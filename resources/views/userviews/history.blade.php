@extends('layouts.app')
@section('content')
	
	<h1 class="text-center py-5">Order History</h1>

	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<table class="table table-striped border">
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Date</th>
							<th>Dish</th>
							<th>From</th>
							{{-- <th>Payment</th>
							<th>Order Status</th> --}}
							
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $order)
						@if($order->user_id = Auth::user()->id)
						<tr>
							<td>{{$order->created_at->format('U')}}-{{$order->id}}</td>
							<td>{{$order->created_at->diffForHumans()}}</td>
							<td>{{$order->product->name}}</td>
							<td>{{$order->product->user->name}}</td>
{{-- 							<td>
								@foreach($order->product_id as $product)
								Name: {{$product->name}}, Qty: {{$product->pivot->quantity}}
								<br>
								@endforeach
							</td> --}}
							{{-- <td>{{$order->payment->name}}</td> --}}
							{{-- <td>{{$order->status->name}}</td>
							<td>
								@if($order->status_id == 3 || $order->status_id == 4)
								@else
								<a href="/cancelorder/{{$order->id}}" class="btn btn-danger">Cancel Order</a>
								@endif
							</td> --}}
						</tr>
						@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

@endsection