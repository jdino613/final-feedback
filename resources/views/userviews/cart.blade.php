@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Cart</h1>

@if($products != null)
<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Dish Name:</th>
						<th>Quantity:</th>
						<th>Total</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
					<tr>
						<td>{{$product->name}}</td>
						<td>{{$product->quantity}}</td>
						<td>{{$product->subtotal}}</td>
						<td>
							<form action="/removeproduct/{{$product->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger" type="submit">Remove from Cart</button>
							</form>
						</td>
					</tr>
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td>Total: {{$total}}</td>
						<td>
							<a href="/clearcart" class="btn btn-danger">Empty Cart</a>
						</td>
						<td></td>
						
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							
                            <form method="POST" action="{{route('OrderController.store')}}">
                                @csrf
                                <input name = "user_id" value = "{{Auth::user()->id}}" hidden>
                                <input name = "product_id" value = "{{$product->id}}" hidden>
                                <input name = "restaurant_id" value="{{$product->user_id}}"hidden>
                                <input name = "product_name" value = "{{$product->name}}" hidden>
                                
                                <input name = "user_address" value = "{{Auth::user()->address}}" hidden>
                            <button type="submit" class="btn btn-primary" value="Order" style="background-color: #32AC71; border: none">
                                <img src="{{ asset('icons/noun_shop.png') }}" alt="Purchase Icon" style="height: 20px; width: 16px; vertical-align: text-bottom;">
                                Place Order
                            </button>
                            </form>
                        
						</td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@else

	<h2 class="text-center py-5">CART IS EMPTY!</h2>

@endif

@endsection