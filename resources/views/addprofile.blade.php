@extends('layouts.app')
@section('content')




<h1 class="text-center">Profile</h1>

	<div class="col-lg-8 offset-lg-2">
		<form action="/add_profile" method="POST" class="form-group">
			@csrf
			
			<div class="my-1">
				<label for="phone">Phone number</label>
				<input type="number" name="phone" class="form-control">
			</div>
			<div class="my-1">
				<label for="hours">Hours</label>
				<input type="hours" name="hours" class="form-control">
			</div>
			<div class="my-1">
				<label for="description">Description</label>
				<textarea name="description" class="form-control" rows="5"></textarea>
			</div>
			<div class="text-center">
				<button class="btn btn-info my-3">Add Profile</button>
			</div>
		</form>
	</div>

@endsection