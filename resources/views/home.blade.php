@extends('layouts.app')

@section('title')
    HomePage
@endsection

@section('content')


 





    <div class="mainPageContent">
     

    <div class="container">

    <div class="row">
        <div class="col-xl" style="padding-top: 20px;">
            <div class="col-lg-5">
                <form class="p-3" action="/search" method="POST">
                    @csrf
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Enter Restaurant Name">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">Find Restaurant</button>
                        </div>
                    </div>
                </form>
            </div>
            @foreach ($restaurants ?? '' as $restaurant)
            {{-- @dd($restaurant) --}}

                <div class="card" style="width: 18rem; display:inline-block; margin-left: 57px; margin-top: 20px;">
                    {{-- @dd(asset("/images/".$restaurant->image)) --}}
                    <img class="card-img-top" src="{{asset($restaurant->image)}}" alt="Card image cap" style="height: 300px;">
                    
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ url('/restaurant/'.$restaurant->id) }}">{{$restaurant->name}}</a></h5>
                        <p class="card-text">{{$restaurant->address}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script type="text/javascript">


    const filter = () => {
        // alert('hi');
        const catId = document.querySelector('#categoryFilter').value;
        // alert(catId);

        window.location.replace('/catalog/'+catId);
    }

    const addToCart = (id, productName) =>{
        console.log(productName)
        let quantity = document.querySelector("#quantity_"+id).value;
        alert(quantity + " of product " + productName + " has been added to cart");

        // let data = new FormData;

        // data.append("_token", "{{ csrf_token() }}");
        // data.append("quantity", quantity);

        fetch("/addtocart/"+id,{
            method: "POST",
            body: {
                "_token": {{csrf_token()}},
                "quantity": quantity
            }
        }).then(res=>res.text())
        .then(res=>console.log(res))
    }

</script>

@endsection