@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="feedback-form" method="post" action="{{ route('feedback.store') }}" >
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                        <div class="row" style="padding: 10px;">
                            <div class="form-group">
                                <textarea class="form-control" name="feedback" placeholder="Write something from your heart..!"></textarea>
                            </div>
                        </div>
                        <div class="row" style="padding: 0 10px 0 10px;">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-lg" style="width: 100%" name="submit">
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
         <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">feedback</div>

                <div class="panel-body feedback-container" >
                    
                    @foreach($feedbacks as $feedback)
                        <div class="well">
                            <i><b> {{ $feedback->name }} </b></i>&nbsp;&nbsp;
                            <span> {{ $feedback->feedback }} </span>
                            <div style="margin-left:10px;">
                                <a style="cursor: pointer;" cid="{{ $feedback->id }}" name_a="{{ Auth::user()->name }}" token="{{ csrf_token() }}" class="feedback_reply">feedback_reply</a>&nbsp;
                                <a style="cursor: pointer;"  class="delete-feedback" token="{{ csrf_token() }}" feedback-did="{{ $feedback->id }}" >Delete</a>
                                <div class="feedback_reply-form">
                                    
                                    <!-- Dynamic feedback_reply form -->
                                    
                                </div>
                                @foreach($feedback->feedback_replies as $rep)
                                     @if($feedback->id === $rep->feedback_id)
                                        <div class="well">
                                            <i><b> {{ $rep->name }} </b></i>&nbsp;&nbsp;
                                            <span> {{ $rep->feedback_reply }} </span>
                                            <div style="margin-left:10px;">
                                                <a rname="{{ Auth::user()->name }}" rid="{{ $feedback->id }}" style="cursor: pointer;" class="feedback_reply-to-feedback_reply" token="{{ csrf_token() }}">feedback_reply</a>&nbsp;<a did="{{ $rep->id }}" class="delete-feedback_reply" token="{{ csrf_token() }}" >Delete</a>
                                            </div>
                                            <div class="feedback_reply-to-feedback_reply-form">
                                    
                                                <!-- Dynamic feedback_reply form -->
                                                
                                            </div>
                                            
                                        </div>
                                    @endif 
                                @endforeach
                                
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

   

</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('/js/main.js') }}">
  
</script>


