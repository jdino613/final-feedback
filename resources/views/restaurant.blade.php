@extends('layouts.app')

@section('title')
    Restaurants
@endsection

@section('content')
    <!-- When User is a guest -->
    @if(Auth::guest())
    <div class="row">
        <div class="container" style="padding-top: 20px; margin-top: 20px; text-align: center;">
            <h2 style="">{{$restaurant->name}}</h2>
                <p>{{$restaurant->address}}</p>
    
                <div class="col-xl" style="padding-top: 20px; padding-bottom: 20px;">
                    @forelse($products as $product)
                        <div class="card" style="width: 18rem; display:inline-block; margin-right: 10px; margin-top: 20px;">
                            <img style="height: 300px; width: 286px; object-fit: contain'" class="card-img-top" src="{{ asset($product->image) }}" alt="Card image cap">
                            <div class="card-body">
                                {{-- <h5 class="card-title"><a href="{{ url('/restaurant/'.$restaurant['id']) }}">{{$product->name}}</a></h5> --}}
                                <p class="card-text">{{$product->name}}</p>
                        
                            </div>
                        </div>
            @empty
                    <p>No food has been created for this {{$restaurant->name}}. Please try again later.</p>
            @endforelse
        </div>


    {{$products->links()}}
</div>
    @else

    <!-- When User is logged in -->
    @if(Session::has("message"))
    <h4 class="alert alert-success text-align text-center" role="alert">{{Session::get('message')}}</h4>
    @endif

    <div class="container" style="padding-top: 20px; margin-top: 20px; text-align: center; text-decoration: none;">
        <h2 style="text-decoration: none;">{{$restaurant->name}}</h2>
        <p>{{$restaurant->address}}</p>
    

        <div class="col-xl" style="padding-top: 20px; padding-bottom: 20px;">
            @forelse($products as $product)
                <div class="card" style="width: 18rem; display:inline-block; margin-right: 10px; margin-top: 20px;">
                    <img class="card-img-top" src="{{ asset($product->image) }}" style="height: 300px; width: 286px;" alt="Card image cap">
                    <div class="card-body">
                        {{-- <h5 class="card-title"><a href="{{ url('/restaurant/'.$restaurant['id']) }}">{{$restaurant ->name}}</a></h5> --}}
                        <p class="card-text">{{$product->name}}</p>

                        
                        @if((Auth::user()->id) != $restaurant->id)
                            <form action="/addtocart/{{$product->id}}" method="POST">
                                @csrf
                                <input type="number" name="quantity" class="form-control" value="1">
                                <input name = "user_id" value = "{{Auth::user()->id}}" hidden>
                                <input name = "product_id" value = "{{$product->id}}" hidden>
                                <input name = "restaurant_id" value="{{$product->user_id}}"hidden>
                                <input name = "product_name" value = "{{$product->name}}" hidden>
                                
                                <input name = "user_address" value = "{{Auth::user()->address}}" hidden>
                            <button type="submit" class="btn btn-primary" value="Order" style="background-color: #32AC71; border: none">
                                <img src="{{ asset('icons/noun_shop.png') }}" alt="Purchase Icon" style="height: 20px; width: 16px; vertical-align: text-bottom;">
                                Add to Cart
                            </button>
                            </form>
                        @endif
                    </div>
                </div>

                @if((Auth::user()->id) == $restaurant->id)
                    <form method="POST" action="{{ url('product/'.$product['id']) }}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                    <a class="btn btn-primary" href="{{url('/product/'.$product['id'].'/edit')}}" style="border: none;">Edit Product</a>
                        <input name="rest" type="hidden" value="{{ $product['user_id'] }}">
                        <input type="submit" value="Delete" style="background-color: red; color: white; border: none;" />
                    </form>
                @endif
            @empty
                    <p>No food has been created for this {{$restaurant->name}}. Please try again later.</p>
            @endforelse
        </div>

    {{$products->links()}}

    </div>
    @endif

    <script type="text/javascript">


    const filter = () => {
        // alert('hi');
        const catId = document.querySelector('#restaurantFilter').value;
        // alert(catId);

        window.location.replace('/restaurant/'+catId);
    }

    const addToCart = (id, productName) =>{
        console.log(productName)
        let quantity = document.querySelector("#quantity_"+id).value;
        alert(quantity + " of product " + productName + " has been added to cart");


        fetch("/addtocart/"+id,{
            method: "POST",
            body: {
                "_token": {{csrf_token()}},
                "quantity": quantity
            }
        }).then(res=>res.text())
        .then(res=>console.log(res))
    }

</script>
@endsection