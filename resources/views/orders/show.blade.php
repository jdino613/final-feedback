@extends('layouts.app')

@section('title')
    Orders Show
@endsection('title')

@section('content')

    <div class="container" style="border:1px solid #cecece; padding-top: 20px; margin-top: 20px; text-align: center;">
        <div class="col-xl" style="padding-top: 20px; padding-bottom: 20px;">
            <div class="alert alert-success" role="alert">
                Your order has been successful! Here are your order details!
            </div>
            @if ($order)
            
                <div class="card" style="width: 18rem; display:inline-block; margin-right: 10px; margin-top: 20px;">
                    @if ($product ?? '')
                    <img class="card-img-top" src="{{asset($product ?? ''->image)}}" alt="Card image cap">
                    @endif
                    <div class="card-body">
                        <p class="card-text">Dish Name: {{ $order->product}}</p>
                      
                        <p class="card-text">Ordered By :  {{ $order->user }}</p>
                        <p class="card-text">Pickup Address : {{ $order->address }}</p>
                        <p class="card-text">Order Date Time : {{$order->time }}</p>
                    </div>
                    
                </div>
            @else
                    <p>Error no invalid order id. Please try again!</p>
            
            @endif
        </div>

@endsection('content')