@extends('layouts.app')

@section('title')
    Orders Index
@endsection

@section('content')

    <div class="container" style="padding-top: 20px; margin-top: 20px; text-align: center;">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Dish Name:</th>
                    <th>Ordered By:</th>
                    {{-- <th>Quantity:</th> --}}
                    <th>Order Date Time:</th>
                    {{-- <th>Order Status:</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                    @if($order->product->user->id == Auth::id())
                    <tr>

                        <td>{{$order->product->name}}</td>

                        <td>{{$order->user->name}}</td>
                        {{-- <td>{{$order->product->quantity}}</td> --}}
                        {{-- <td>{{$order->address}}</td> --}}
                        <td>{{$order->created_at}}</td>
                    </tr>
                    @endif
                @endforeach

            </tbody>
        </table>

    </div>


@endsection('content')