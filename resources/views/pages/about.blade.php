@extends('layouts.app2')

@section('title')
    About Us
@endsection

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">


@section('content')


<div class="py-12">

        <h2 class="text-center uppercase text-2xl lg:text-4xl tracking-wider mb-6">A movement bigger than us</h2>

        <p class="text-gray-700 mx-auto max-w-4xl text-center leading-relaxed mb-2">Food waste is disrespectful on so many levels: to the hungry, to the people that spend their time cooking, to our natural resources... yet is a practice that is generally accepted and this is what makes fighting it so challenging.
<br><br>
        An estimated 2,175 tons of food scraps end up in trash bins on a daily basis in Metro Manila. 2.4 million Filipino families experienced involuntary hunger at least once in the past three months. These sobering statistics give us a glimpse of the reality that families are facing when it comes to the major hunger issue we are experiencing in the Philippines. And yet, it has been estimated that each Filipino still wastes an average of 3.29 kg of rice per year, which, when totaled, would be enough to feed 4.3 million Filipinos.
<br><br>
        On a global scale, 1/3 of the food produced globally every year never reaches our plates. According to the Food and Agriculture Organization (FAO), if food waste were a country, it would be the third largest greenhouse emitter, just after the United States and China.
		There is clearly a gap between the amount of food we are able to produce as a country and the amount of food made available for our citizens to consume. That gap is actually the amount of food that we knowingly or unknowingly waste.
<br><br>
		That is the reason why FeedBack is conceived. We believe the solution is simple: feed more, waste less. FeedBack provides a platform for charities, humanitarian organizations, orphanages, food banks, low-income public schools, and community kitchens to connect with restaurants, fast food companies, buffets, caterers, and food retailers to help manage food waste from the day's food surplus. The FeedBack model aims to provide a triple-win solution by improving waste management, reducing its greenhouse emissions from landﬁlls and getting its edible surplus food to those in need.</p>

</div>

@endsection
