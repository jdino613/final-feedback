@extends('layouts.app')

@section('title')
    Edit Product
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="padding-top:20px;">
            <div class="card">
                <div class="card-header"  style="text-align: center;">{{ __('EDIT EXISTING DISH') }}</div>

                @if(count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card-body">
                    <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}" enctype="multipart/form-data">
                        @csrf
                        {{method_field('PUT')}}
                        <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
                        
                        <div class="form-group row">
                            <label for="product_name" class="col-md-4 col-form-label text-md-right">{{ __('Dish Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="name" value="{{$product->getattribute('name')}}" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="image">Image 
                                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
                                </label>
                                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                            </div>  
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" value="Add New Food Listing" class="btn btn-primary">
                                    {{ __('Update Food Listing') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <h3 style="text-align: center; padding-top: 40px; margin-top: 30px;">EDIT EXISTING DISH {{strtoupper(Auth::user()->name)}}</h3>
<div id="create">
<div class="container" style="padding-top: 20px; margin-top: 30px; margin-left: 27rem;">
    
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}" enctype="multipart/form-data">
       @csrf
        {{method_field('PUT')}}

        <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
        <div class="form-group">
          <label class="col-md-4 control-label" for="product_name">DISH NAME</label>  
              <div class="col-md-4">
                <input value="{{old('name')}}" id="name" name="name" placeholder="DISH NAME" class="form-control input-md" required="" type="text">
            </div>
        </div>
    <div class="form-group">
        <div class="col-md-4">
        <label class="col-md-4 control-label" for="image">IMAGE</label> 
          <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        </div>
    </div>
        
        <div class="form-group">        
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" type="submit" value="Add New Food Listing" class="btn btn-primary">EDIT</button>
          </div>
        </div>
        
    </form>
    </div>

</div> --}}



    {{-- <h3>Edit existing dish</h3>
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <p>
            <label>Name: </label>
            <input type="text" name="name" value="{{$product->getattribute('name')}}"/>
        </p>
            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        
        <input type="submit" value="Submit changes" />
    </form> --}}
@endsection