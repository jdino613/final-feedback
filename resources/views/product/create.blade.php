@extends('layouts.app')

@section('title')
    Add New Food Listing
@endsection

@section('content')

<h3 style="text-align: center; padding-top: 40px; margin-top: 30px;">ADD NEW FOOD LISTING FOR {{strtoupper(Auth::user()->name)}}</h3>
<div id="create">
<div class="container" style="padding-top: 20px; margin-top: 30px; margin-left: 27rem;">
    
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form method="POST" action="{{url('product')}}" enctype="multipart/form-data">
       @csrf

        <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
        <div class="form-group">
          <label class="col-md-4 control-label" for="product_name">DISH NAME</label>  
          <div class="col-md-4">
          <input value="{{old('name')}}" id="name" name="name" placeholder="DISH NAME" class="form-control input-md" required="" type="text">
      </div>
  </div>
    <div class="form-group">
        <div class="col-md-4">
        <label class="col-md-4 control-label" for="image">IMAGE</label> 
          <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        </div>
    </div>
        
        <div class="form-group">        
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" type="submit" value="Add New Food Listing" class="btn btn-primary">ADD TO LISTING</button>
          </div>
        </div>
        
    </form>
    </div>

</div>




    {{-- <div class="container" style="padding-top: 20px; margin-top: 20px;">
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h3 style="text-align: center; padding-top: 20px;">Add new food listing for {{Auth::user()->name}}</h3>
    <form method="POST" action="{{url('product')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
        <p><label>Name: </label><input type="text" name="name" value="{{old('name')}}" /></p>
        
        <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        <input type="submit" value="Add New Food Listing" style="margin-bottom: 20px;" />
    </form>
    </div> --}}
@endsection