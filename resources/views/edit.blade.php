@extends('layouts.app')
@section('content')


{{-- @if(Session::has("message"))
    <h4 class="alert alert-success text-align" role="alert">{{ Session::get('message') }}</h4>
@endif --}}


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="padding-top:20px;">
            <div class="card">
                <div class="card-header"  style="text-align: center;">{{ __('Account Settings') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{route('update', $user)}}" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('patch') }}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="name" value="{{ $user->name }}" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="image" id="imageFile" aria-describedby="fileHelp">
                                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="email">Email address:</label>
                            <div class="col-md-6">                           
                              <input type="email" name="email" class="form-control" value="{{ $user->email }}" id="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" placeholder="Enter password" name="password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="password">Confirm Password:</label>
                            <div class="col-md-6"> 
                              <input type="password" placeholder="Confirm password" class="form-control" type="password" name="password_confirmation" id="password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save Changes') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- <form method="post" action="{{route('update', $user)}}" enctype="multipart/form-data">
    @csrf
    {{ method_field('patch') }}
    <h3>Edit Profile</h3>

<div class="profile-header-container">
                <div class="profile-header-img">
                    
                    <div class="rank-label-container">
                        <span class="label label-default rank-label">{{$user->name}}</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row justify-content-center"> --}}
           {{--  <form action="{{ url('edit/{user}') }}" method="post" enctype="multipart/form-data">
                @csrf --}}
                {{-- <div class="form-group">
                    <input type="file" class="form-control-file" name="image" id="imageFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                </div> --}}
                {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
            {{-- </form> --}}
        {{-- </div> --}}
    {{-- </div> --}}


@endsection